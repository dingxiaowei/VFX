﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class TestVFX : MonoBehaviour
{
    void Start()
    {

    }

    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 100, 20), "Test"))
        {
            VisualEffectAsset asset = Resources.Load<VisualEffectAsset>("VFX/GrenadeHole");
            VFXManager.Instance.SpawnPointEffect(asset, Vector3.zero, Vector3.one);
        }
    }

}
