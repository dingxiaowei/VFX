﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.VFX;

public class VFXEffectTypeData
{
    public VisualEffect Effect;
    public float MaxDuration = 4.0f;
    public bool Active;
    public long LastTriggerTime;
    public VFXEventAttribute EventAttribute;
}

public struct PointEffectRequest
{
    public Vector3 mPosition;
    public Vector3 mNormal;
    public VisualEffectAsset mAsset;
}

public struct LineEffectRequest
{
    public Vector3 mStart;
    public Vector3 mEnd;
    public VisualEffectAsset mAsset;
}
